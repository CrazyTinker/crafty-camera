var crafty_camera = window.crafty_camera || {
    'last_snapshot': 'NULL',
    
    'init': function() {
        background();
        this.webcam();
    },
    
    'webcam': function() {
        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach( '#crafty_camera' );
		this.overlay();
    },
    
    'overlay': function() {
        var camera_overlay = document.getElementById('camera_overlay');
        var context = camera_overlay.getContext('2d');
        var centerX = camera_overlay.width / 2;
        var centerY = camera_overlay.height / 2;
        var radius = 70;

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.lineWidth = 3;
        context.strokeStyle = '#F00';
        context.stroke();
    },
    
    'shutter': function() {
        // preload shutter audio clip
        var shutter = new Audio();
        shutter.autoplay = false;
        shutter.src = navigator.userAgent.match(/Firefox/) ? 'sfx/shutter.ogg' : 'sfx/shutter.mp3';
        shutter.play();
    },
    
    'take_snapshot': function() {
        // play sound effect
        this.shutter();
        this.last_snapshot = new Date().getTime();
        
        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
            // display results in page
            document.getElementById('results').innerHTML = 
            '<img id="'+ crafty_camera.last_snapshot +'" src="'+data_uri+'"/>';
            crafty_camera.craftify(crafty_camera.last_snapshot);
        } );
    },
    
    'craftify': function(image_id) {
        document.getElementById(image_id).closePixelate([
            { shape: 'square', resolution: this.settings.resolution,
              size: this.settings.size, offset: 0, alpha: 0.991 },
            { shape: 'square', resolution: this.settings.resolutionModifier,
              size: this.settings.sizeModifier, offset: 0, alpha: 0.991 }
        ]);
    },
    
    'settings': {
        resolution: '200',
        size: '200',
        resolutionModifier: '8',
        sizeModifier: '10'
    }
}

crafty_camera.init();
